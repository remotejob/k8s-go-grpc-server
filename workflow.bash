go mod edit -go=1.20
go get -u all
go mod tidy


k config use-context oracle
k config set-context --current --namespace=demoproject

kubectl port-forward -n demoproject svc/k8s-go-grpc-server-svc 7010
curl http://localhost:7010/grpc/api/1/3


