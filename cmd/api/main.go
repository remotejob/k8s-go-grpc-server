package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"
	"time"

	"github.com/gorilla/mux"
	"github.com/rs/cors"
	"gitlab.com/remotejob/k8s-go-grpc-server/pkg/pd"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
)

/**
 * :=  created by:  Mazurov
 * :=  create date:  Fri 09 Dec 2022
 * :=  (C) CopyRight Mazurov
 * :=  mazurov.eu
 * :=  Fun  :  Coffee  :  Code
 **/

func main() {
	//	Connect to Add service
	conn, err := grpc.Dial("add-service:3000", grpc.WithInsecure())
	if err != nil {
		panic(err)
	}
	addClient := pb.NewAddServiceClient(conn)

	routes := mux.NewRouter()

	routes.HandleFunc("/grpc/api/{a}/{b}", func(w http.ResponseWriter, r *http.Request) {

		w.Header().Set("Content-Type", "application/json; charset=UFT-8")

		vars := mux.Vars(r)
		a, err := strconv.ParseUint(vars["a"], 10, 64)
		if err != nil {
			json.NewEncoder(w).Encode("Invalid parameter A")
		}
		b, err := strconv.ParseUint(vars["b"], 10, 64)
		if err != nil {
			json.NewEncoder(w).Encode("Invalid parameter B")
		}

		ctx, cancel := context.WithTimeout(context.TODO(), time.Minute)
		defer cancel()

		req := &pb.AddRequest{A: a, B: b}
		if resp, err := addClient.Compute(ctx, req); err == nil {
			msg := fmt.Sprintf("a=%d b=%d Test OK a + b = %d !!!", a, b, resp.Result)
			json.NewEncoder(w).Encode(msg)
		} else {
			msg := fmt.Sprintf("Internal server error: %s", err.Error())
			json.NewEncoder(w).Encode(msg)
		}
	}).Methods("GET")

	fmt.Println("Application is running on : 7010 .....")
	handler := cors.Default().Handler(routes)
	http.ListenAndServe(":7010", handler)
}
