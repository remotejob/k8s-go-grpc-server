module gitlab.com/remotejob/k8s-go-grpc-server

go 1.20

require (
	github.com/golang/protobuf v1.5.3
	github.com/gorilla/mux v1.8.0
	golang.org/x/net v0.17.0
	google.golang.org/grpc v1.59.0
	google.golang.org/protobuf v1.31.0
)

require (
	github.com/rs/cors v1.10.1 // indirect
	golang.org/x/sys v0.13.0 // indirect
	golang.org/x/text v0.13.0 // indirect
	google.golang.org/genproto/googleapis/rpc v0.0.0-20231016165738-49dd2c1f3d0b // indirect
)
